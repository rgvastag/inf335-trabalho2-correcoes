package br.unicamp.ic.inf335.beans;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

public class ProdutoBeanTeste {

	@Test
	public void testCriacaoProduto() {
		ProdutoBean produto = new ProdutoBean("1", "Tênis Um", "Descrição do tênis um", 100.99, "Estado um");

		assertEquals(produto.getCodigo(), "1");
		assertEquals(produto.getNome(), "Tênis Um");
		assertEquals(produto.getDescricao(), "Descrição do tênis um");
		assertEquals(produto.getValor(), 100.99);
		assertEquals(produto.getEstado(), "Estado um");
	}

	@Test
	public void testOrdenarProdutosPorMenorValor() {
		List<ProdutoBean> produtos = new ArrayList<>();
		produtos.add(new ProdutoBean("1", "Tênis Um", "Descrição do tênis um", 100.99, "Estado um"));
		produtos.add(new ProdutoBean("2", "Tênis Dois", "Descrição do tênis dois", 1.99, "Estado Dois"));
		produtos.add(new ProdutoBean("3", "Tênis Três", "Descrição do tênis três", 250.00, "Estado Três"));

		Collections.sort(produtos);

		assertEquals(produtos.get(0).getNome(), "Tênis Dois");
		assertEquals(produtos.get(0).getValor(), 1.99);

		assertEquals(produtos.get(2).getNome(), "Tênis Três");
		assertEquals(produtos.get(2).getValor(), 250.00);
	}

	@Test
	public void testValorNegativo() {
		assertThrows(IllegalArgumentException.class, () -> new ProdutoBean("1", "Tênis Um", "Descrição do tênis um", -10.0, "Estado um"));
	}
}
